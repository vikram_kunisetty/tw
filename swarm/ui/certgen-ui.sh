mkdir -p /etc/ssl/private /etc/ssl/certs
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/ui.key -out /etc/ssl/certs/ui.crt -config /tmp/ui.conf -batch