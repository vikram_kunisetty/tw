The News Company Installation
==============================

Hosting Options
-----------------
1. Docker Swarm 
2. Vagrant and Ansible


Using Docker Swarm
-------------------
Spins up two containers - one for the app(hosted on tomcat) and the other for ui serving static content securely. 
- Pre-Requisites (Bare Minimum/Simplest)
    - Docker CE is installed 
    - Docker swarm is initialized with ```docker swarm init```
    - Apache Bench Mark Tool (Optional)

- Steps:
    - Build the images
        ```
        cd swarm/app && dockerfile build -t mywebapp .
        cd swarm/ui && dockerfile build -t ui .
        ```
    - Deploy the stack on swarm:
    ``` cd swarm
        docker stack deploy -c docker-stack.yml companyNews
    ```
    - Scaling Up to the desired state based on the load.
    ```
        docker service scale companyNews_app=2
        docker service scale companyNews_ui=2
    ```
    - Optional Step to test to create prevayler records in bulk, check RPS etc
    ```
        echo "title=Tittle1&body=Body" > post.data 
        ab -T 'application/x-www-form-urlencoded'  -n 1000 -c 50 -v 1 -p post.data https://localhost:18443/companyNews/Post.action
        ab -T 'application/x-www-form-urlencoded'  -n 1000 -c 50 -v 1 https://localhost:18443/companyNews/Read.action
    ```
    - Scaled up the app and re check the results.

Using Vagrant and Ansible
----------------------------
Provisions up two nodes (One for the App and the other for ui). 
Ansible Playbook will configure respective components (tomcat, nginx, certificates etc) required to host the app and ui securely using self signed certificates.

- Pre-Requisites (Bare Minimum/Simplest)
    - Vagrant and Oracle Virtualbox are installed 

- Steps:
    - Build Vagratn
    ```
    cd vagrant
    vagrant up
    ```
    - update the hosts file with the following entry: 
        ```10.20.30.41 ui```
    - access the app using the url: https://10.20.30.40:8443/companyNews/


Problems and Improvements
===========================

1. The source code needs to be available properly in the version control - Suggestion would be to create a Repo and link to a CI process to publish to a binary repository
2. Applicaiton currently runs on HTTPS self signed certificates - These certificates needs to be signed by a trust before it goes live. 
3. Include more integration tests, unit tests etc.
4. choosing a tool that will enable auto-scaling etc. 
5. Simple Load test has been performed and the results are in the file `abtest.log`

